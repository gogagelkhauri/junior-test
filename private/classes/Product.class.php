<?php

class Product extends DatabaseObject {

  static protected $table_name = 'products';
  static protected $db_columns = ['id','sku','name','category','description'];

  public $id;
  public $sku;
  public $name;
  public $price;
  public $category;
  public $description;

   public const CATEGORIES = ['Book', 'CD', 'Furniture'];



  public function __construct($args=[]) {
  
    $this->sku = $args['sku'] ?? '';
    $this->name = $args['name'] ?? '';
    $this->price = $args['price'] ?? '';
    $this->category = $args['category'] ?? '';
    $this->description = $args['description'] ?? '';


  }

  protected function validate() {
    $this->errors = [];

    if(is_blank($this->sku)) {
      $this->errors['sku'] = "SKU cannot be blank.";
    }
    if(is_blank($this->name)) {
      $this->errors['name'] = "Name cannot be blank.";
    }
    if(is_blank($this->price)) {
      $this->errors['price'] = "Price cannot be blank.";
    }
    if(is_blank($this->description)) {
      $this->errors['description'] = "Description cannot be blank.";
    }
    
    return $this->errors;
  }

}

?>
