<?php

require_once('../private/initialize.php');


if(!isset($_GET['id'])) {
  redirect_to(url_for('index.php'));
}
$id = $_GET['id'];
$product = Product::find_by_id($id);
if($product == false) {
  redirect_to(url_for('index.php'));
}

if(is_post_request()) {


  $result = $product->delete();
  $session->message('The Product was deleted successfully.');
  redirect_to(url_for('index.php'));

} 

?>

<?php $page_title = 'Delete Product'; ?>
<?php include(SHARED_PATH . '/public_header.php'); ?>

<div id="content">

  <a class="back-link" href="<?php echo url_for('index.php'); ?>">&laquo; Back to List</a>

  <div class="bicycle delete">
    <h1>Delete Product</h1>
    <p>Are you sure you want to delete this product?</p>
    <p class="item">Product name: <?php echo h($product->name); ?></p>

    <form action="<?php echo url_for('delete.php?id=' . h(u($id))); ?>" method="post">
      <div id="operations">
        <input type="submit" name="commit" value="Delete Product" />
      </div>
    </form>
  </div>

</div>

