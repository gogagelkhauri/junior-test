<?php


if(!isset($product)) {
  redirect_to(url_for('index.php'));
}
?>

<div class="form-group">
<label for="product">SKU</label> <br>
<input type="text" name="product[sku]" class="form-control" />
<p class="text-danger"><?php echo isset($product->errors['sku']) ? $product->errors['sku'] : ''; ?></p>
</div>

<div class="form-group">
<label for="name">Name</label> <br>
<input type="text"  name="product[name]" class="form-control"/>
<p class="text-danger"><?php echo isset($product->errors['name']) ? $product->errors['name'] : ''; ?></p>
</div>

<div class="form-group">
<label for="name">Price</label> <br>
<input type="text" placeholder="$" name="product[price]" size="18" class="form-control" />
<p class="text-danger"><?php echo isset($product->errors['price']) ? $product->errors['price'] : ''; ?></p>
</div>


<div class="form-group">
<label for="category">Category</label><br>
<select name="product[category]" id="category" class="form-control">

    <?php foreach(Product::CATEGORIES as $category) { ?>
      <option value="<?php echo $category; ?>" ?><?php echo $category; ?></option>
    <?php } ?>
    </select>
</div>


<div class="form-group">
<label for="description">Description</label> <br>
<textarea rows="5" cols="5" name="product[description]" class="form-control"></textarea>
<p class="text-danger"><?php echo isset($product->errors['description']) ? $product->errors['description'] : ''; ?></p>
</div>

<script>

$(document).ready(function (e) {
    $('#category').change( function () {
        myfun();
    });
});


 
  function myfun(){
    var value = document.getElementById('category').value
    var myform = document.getElementById('myform')
    if(value === 'book'){
      var inp = document.createElement("INPUT");
      inp.setAttribute("placeholder", "book");
      myform.appendChild(inp)
      console.log('123')
    }
  }

  

</script>








