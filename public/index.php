  

<?php $page_title = 'Inventory'; ?>
<?php include('../private/shared/public_header.php'); ?>

<?php require_once('../private/initialize.php'); ?>
<form action="function.php" method="POST">
<button type="submit" class="btn btn-danger mt-3 ml-2" id="delete">Delete Selected</button>
<div class="container">
  <div class="row margin-left">

<?php

$products = Product::find_all();

if(isset($_SESSION['err'] )){
  echo $_SESSION['err'];
}
foreach($products as $product) { ?>

  <div class="col-md-3 border  m-3">
    <input type="checkbox" name="id[<?php echo $product->id; ?>]" id="">
      <div class="mt-3 text-center">
        
        <p><?php echo h($product->sku); ?></p>
        <p><?php echo h($product->name); ?></p>
        <p><?php echo h($product->price); ?></p>
        <p><?php echo h($product->category); ?></p>
        <p><a href="delete.php?id=<?php echo h($product->id); ?>">Delete</a></p>
      </div>
    </div>

<?php } ?>
  </div>
  
</div>
</form>

<?php include('../private/shared/public_footer.php'); ?>

<script>
$(document).ready(function(){
  $('#delete').click(function(){
       if($('input:checkbox:checked').length < 1){
        alert('No record selected ');
       }
})
</script>