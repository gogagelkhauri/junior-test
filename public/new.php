<?php
require_once('../private/initialize.php'); 


if(is_post_request()) {

  // Create record using post parameters
  $args = $_POST['product'];
  $product = new Product($args);
  $result = $product->save();

  if($result === true) {
    $new_id = $product->id;
    redirect_to(url_for('index.php'));
  } else {
    // show errors
  }

} else {

  $product = new Product;
}

?>

<?php $page_title = 'Create Product'; ?>
<?php include(SHARED_PATH . '/public_header.php'); ?>

<div id="content">

  

  <div class="container">
  <a class="btn btn-primary float-right" href="<?php echo url_for('index.php'); ?>">&laquo; Back to List</a>
    <h1>Create Product</h1>


    <form action="<?php echo url_for('new.php'); ?>" method="post" id="myform">

      <?php include_once('form_fields.php'); ?>

      
      <div class="form-group">
        <input type="submit" value="Add Product" class="btn btn-success mt-2 form-control"/>
      </div>
    </form>

  </div>

</div>

<?php require_once('../private/shared/public_footer.php');  ?>